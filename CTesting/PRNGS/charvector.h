/*
 * CharVector.h
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define VECTOR_INITIAL_CAPACITY 4

#ifndef VectorF

#define VectorF
typedef struct {
  int size;
  int capacity;
  char **data;
} Vector;

#endif

void vector_init(Vector *vector);

void vector_append(Vector *vector, char* value);

char* vector_get(Vector *vector, int index);

void vector_set(Vector *vector, int index, char* value);

void vector_double_capacity_if_full(Vector *vector);

void vector_free(Vector *vector);
