/*
 * IOManager.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "IOManager.h"
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdbool.h>
#include <unistd.h>

#include "utils.h"
#include "b64.h"
#include "config.h"

/**
  * Returns true if a file can be opened, read, and written too.
  *
  * @param the path to the file
  * @return True if can open, read, and write. Otherwise false.
*/
bool canUseFile(char* path) {
  return access(path, F_OK|R_OK|W_OK) != -1;
}

/**
  * Creates a new file.
  *
  * @param the Full path of the file
  * @return Nothing
*/
void createNewFile(char* path) {
  if(!canUseFile(path)) {
    FILE *fp = fopen(path, "ab+");
    fclose(fp);
  }
}

/**
  * Adds a line to a file that already exists
  *
  * @param file to write too
  * @param string to write.
  * @return Nothing
*/
void appendToFile(char* fTW, char* toWrite) {
  if(canUseFile(fTW)) {
    FILE *fp = fopen(fTW, "a");
    fprintf(fp, "%s", toWrite);
    fclose(fp);
  }
}

/**
  * Returns all lines in a file in a vector.
  *
  * @param The file to read.
  * @return The lines of a file in a char vector.
*/
Vector getLinesFromFile(FILE *fp) {
	Vector lines;
	vector_init(&lines);
	char buf[256];
	while (fgets (buf, sizeof(buf), fp)) {
    	vector_append(&lines, buf);
  }
  return lines;
}

/**
  * Returns a file's UID if one hasn't been generated. Basically this way we never have a file with the same name.
  *
  * @return FileName
*/
char* genFileName() {
  long l = currentMicroTime();
  char* c = "";
  longToStr(l, c);
  char* uid = md5String(c);
  return uid;
}

/**
  * Delete's a file.
  *
  * @param File path to delete.
  * @return nothing
*/
void deleteFile(char* path) {
  if (setjmp(s_jumpBuffer)) {
	printf("%s", addStrings(2, "FAILED TO REM FILE: ", path));  
  }else {
	unlink(path);
    remove(path);
  }
}

/**
  * Writes a blob file for one that doesn't exist.
  *
  * @param The iteration.
  * @param The algorithim name
  * @param An FloatVector of the data.
  * @return New blob File name.
*/
char* writeBlobFileNew(int Iteration, char* algoName, FloatVector data, char* testType, float mostCommonSeed) {
  Vector datToWrite;
  vector_init(&datToWrite);
  vector_append(&datToWrite, algoName);
  vector_append(&datToWrite, addStrings(4, "----[Starting Blob: " , (char) Iteration , " for: " , algoName , "]----"));
  vector_append(&datToWrite, addStrings(2, "MostCommonSeed: " , (char) mostCommonSeed));
  for(int i = 0; i < data.size - 2; i += 2) {
    if(vector_getfloat(&data, i) != 666) {
      char* x = addStrings(4, "X:" , (char) vector_getfloat(&data, i) , " Y: " , (char) vector_getfloat(&data, i + 1));
      char* y = addStrings(3, "In Quadrant : " , isInQuadrant(vector_getfloat(&data, i), (char) vector_getfloat(&data, i + 1)));
      vector_append(&datToWrite, x);
      vector_append(&datToWrite, y);
    }else {
      if(!((i + 1) == 666)) {
        i++;
        char* x = addStrings(4, "X:" , (char) vector_getfloat(&data, i) , " Y: " , (char) vector_getfloat(&data, i + 1));
        char* y = addStrings(3, "In Quadrant : " , isInQuadrant(vector_getfloat(&data, i), (char) vector_getfloat(&data, i + 1)));
        vector_append(&datToWrite, x);
        vector_append(&datToWrite, y);
      }
    }
  }
  if(atof(vector_get(&datToWrite, datToWrite.size - 1)) != vector_getfloat(&data, data.size - 1)) {
    char* holder = addStrings(2, "MostCommonSeed: " , (char) vector_getfloat(&data, data.size - 1));
    vector_append(&datToWrite, holder);
  }
  vector_append(&datToWrite, addStrings(5, "----[Ending Blob: " , (char) Iteration , " for: " , algoName , "]----"));
  char* dat = charVecToString(datToWrite, "");
  char* encoded = "";
  b64_encode(dat, encoded);
  char* fname = addStrings(6, BASE_DIR , "uncomp/" , testType , "/" , genFileName() , ".txt");
  createNewFile(fname);
  appendToFile(fname, encoded);
  return fname;
}

/**
  * Writes a blob file for one that does exist.
  *
  * @param The iteration.
  * @param The algorithim name
  * @param An FloatVector of the data.
  * @param the name of the file
  * @return nothing
*/
void writeBlobFile(int Iteration, char* algoName, FloatVector data, char* fname, float mostCommonSeed) {
 Vector datToWrite;
  vector_init(&datToWrite);
  vector_append(&datToWrite, algoName);
  vector_append(&datToWrite, addStrings(4, "----[Starting Blob: " , (char) Iteration , " for: " , algoName , "]----"));
  vector_append(&datToWrite, addStrings(2, "MostCommonSeed: " , (char) mostCommonSeed));
  for(int i = 0; i < data.size - 2; i += 2) {
    if(vector_getfloat(&data, i) != 666) {
      char* x = addStrings(4, "X:" , (char) vector_getfloat(&data, i) , " Y: " , (char) vector_getfloat(&data, i + 1));
      char* y = addStrings(3, "In Quadrant : " , isInQuadrant(vector_getfloat(&data, i), (char) vector_getfloat(&data, i + 1)));
      vector_append(&datToWrite, x);
      vector_append(&datToWrite, y);
    }else {
      if(!((i + 1) == 666)) {
        i++;
        char* x = addStrings(4, "X:" , (char) vector_getfloat(&data, i) , " Y: " , (char) vector_getfloat(&data, i + 1));
        char* y = addStrings(3, "In Quadrant : " , isInQuadrant(vector_getfloat(&data, i), (char) vector_getfloat(&data, i + 1)));
        vector_append(&datToWrite, x);
        vector_append(&datToWrite, y);
      }
    }
  }
  if(atof(vector_get(&datToWrite, datToWrite.size - 1)) != vector_getfloat(&data, data.size - 1)) {
    char* holder = addStrings(2, "MostCommonSeed: " , (char) vector_getfloat(&data, data.size - 1));
    vector_append(&datToWrite, holder);
  }
  vector_append(&datToWrite, addStrings(5, "----[Ending Blob: " , (char) Iteration , " for: " , algoName , "]----"));
  char* dat = charVecToString(datToWrite, "");
  char* encoded = "";
  b64_encode(dat, encoded);
  appendToFile(fname, encoded);
}
