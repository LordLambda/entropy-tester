/*
 * Utils.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>
#include "md5.h"
#include "charvector.h"
#include "floatvector.h"

/**
  * Return 0 if the string haystack ends with the string needle
  * 
  * @param haystack the string to be analyzed
  * @param needle the suffix string
  * @return 0 if the string haystack ends with the string needle, 1 if not
*/
int endWith(const char *haystack, const char *needle) {
    int blen = strlen(haystack);
    int slen = strlen(needle);
    return (blen >= slen) && (0 == strcmp(haystack + blen - slen, needle));
}

/**
  * Checks if two floats are "in a quadrant". See the randomPie test for it in use.
  *
  * @param The first float
  * @param The second float
  * @return True if in quadrant. Else return false.
*/
bool isInQuadrant(float x, float y) {
	float distance = sqrt((x * x) + (y * y));
	return distance <= 1;
}

/**
  * Returns the md5 of a "String"
  *
  * @param String to md5
  * @return md5 as a string
*/
char* md5String(char* s) {
  md5_state_t md5Instance;
  md5_init(&md5Instance);
  char* retV;
  const md5_byte_t a = (md5_byte_t) s;
  md5_append(&md5Instance, &a, strlen(s));
  md5_finish(&md5Instance, (md5_byte_t**) &retV);
  return retV;
}

/**
  * Returns the current microsecond time.
  *
  * @return microtime
*/
long currentMicroTime() {
  struct timeval tv;
  gettimeofday(&tv,NULL);
  uint64_t micro = tv.tv_sec*(uint64_t)1000000+tv.tv_usec;
  return (unsigned long) micro;
}

/**
  * Turns a long into a string.
  *
  * @param The long to convert.
  * @param The char to store.
  * @return Nothing
*/
void longToStr(long n, char* pStr)
{
  int i = 0;
  int m;
  int len;
  char c;
  char s = '+';
 
  if( n < 0 )
  {
    s = '-';
    n = - n;
    pStr[0]='-';
    i++;
  }
 
  do
  {
    m = n % (long)10;
    pStr[i] = '0'+ m;
    n = n / (long)10;
    i++;
  }
  while(n != 0);
 
  if(s == '+')
  {
    len = i;
  }
  else /* s=='-' */
  {
    len = i-1;
    pStr++;
  }
 
  for(i=0; i<len/2; i++)
  {
    c = pStr[i];
    pStr[i]       = pStr[len-1-i];
    pStr[len-1-i] = c;
  }
  pStr[len] = 0;
 
  if(s == '-')
  {
    pStr--;
  }
}

/**
  * Turns a array of chars or 'String' into an vector of the chars ASCII values.
  *
  * @param String to convert.
  * @return vector
*/
Vector stringToAsciiValue(char* str) {
  Vector retV;
  vector_init(&retV);
  for(int i = 0; i < strlen(str); ++i) {
	  char cc = (char) ((int) str[i]);
    vector_append(&retV, &cc);
  }
  return retV;
}

/**
  * Turns a charvector into one giant string 
  *
  * @param Vector of data.
  * @param delimter between strings
  * @return the vector condensed.
*/
char* charVecToString(Vector dat, char* delim) {
  char* retV = "";
  for(int i = 0; i < dat.size; ++i) {
    char* a = vector_get(&dat, i);
    if(a != NULL) {
      char* holder = malloc(strlen(retV) + strlen(a) + strlen(delim));
      strcpy(holder, retV);
      strcat(holder, a);
      strcat(holder, delim);
      retV = holder;
    }
  }
  return retV;
}

/**
  * Deletes a variable in a vector, and shifts all the values forward to fill it's space.
  *
  * @param the number to delete. Non-Array counting.
  * @oaram the vector to modify.
  * @return the shifted vector
*/
Vector deleteAndShift(int numToDel, Vector vec) {
  Vector vecc;
  vector_init(&vecc);
  for(int i = 0; i < vec.size; ++i) {
    if(i != (numToDel + 1)) {
      vector_append(&vecc, vector_get(&vec, i));
    }
  }
  vector_free(&vec);
  return vecc;
}

/**
  * Clears all of a float vectors data.
  *
  * @param Vector to clear
  * @return Nothing
*/
void clearFloatVector(FloatVector *vec) {
  vector_freefloat(vec);
  vector_initfloat(vec);
}

/**
  * Clears all of a char vectors data.
  *
  * @param Vector to clear
  * @return Nothing
*/
void clearCharVector(Vector *vec) {
  vector_free(vec);
  vector_init(vec);
}

/**
  * Returns the average of a float vector.
  *
  * @param The vector to average
  * @return The average of the vector
*/
float averageFloatVector(FloatVector vec) {
  int realCount = 0;
  float retV = 0;
  for(int i = 0; i < vec.size; ++i) {
    if(vector_getfloat(&vec, i) != 666) {
      retV += vector_getfloat(&vec, i);
      realCount++;
    }
  }
  retV /= realCount;
  return retV;
}

/**
  * Splits a 'string' => Char Array by a delimeter
  *
  * @param The string to split
  * @param The delimeter to split by.
  * @return The split string
*/
char* splitByDelimeter(char* toSplit, char* delimeter) {
  return strtok(toSplit, delimeter);
}

/**
  * Adds to 'char*' s together. For ease of use.
  * 
  * @param The first string
  * @param The second string
  * @return The added String
*/
char* addStrings(int numOfArgs, char* one, ...) {
	va_list argumentsToPass;
    va_start(argumentsToPass, one);
    char** list = malloc(numOfArgs);
    for(int n = 0; n < numOfArgs; n++)
		list[n] = va_arg(argumentsToPass, char*);
	va_end(argumentsToPass);
	int totalSize = 0;
	for(int i = 0; i < sizeof(list); ++i) {
		char* c = list[i];
		totalSize += sizeof(c);
	}
	char* holder = malloc(totalSize);
	bool hasDone = false;
	for(int ii = 0; ii < sizeof(list); ++ii) {
		if(!hasDone) {
			strcpy(holder, list[ii]);
			hasDone = true;
		}else {
			strcat(holder, list[ii]);
		}
	}
	return holder;
}
