/*
 * Main.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dlfcn.h>
#include <unistd.h>
#include <stdbool.h>

#include "charvector.h"
#include "IOManager.h"
#include "loader.h"
#include "utils.h"
#include "config.h"
#include "floatvector.h"

Vector checkQueue;
char* currentCheck;
float checkTime;
int lastStored;
float averageCheckTime;
FloatVector overallRandoms;
FloatVector randomSeeds;
FloatVector checks;

/**
  * Updates the check queue by checking the check queue file.
  * 
  * @return nothing.
*/
void updCheckQueue(char* path) {
  if(access(path, F_OK|R_OK) == -1) {
    return;
  }
  FILE *fp = fopen("../tocheck.txt", "r");
  Vector vec = getLinesFromFile(fp);
  for(int i = 0; i < vec.size; ++i) {
    char* c = vector_get(&vec, i);
    if(c != NULL) {
      vector_append(&checkQueue, c);
    }
  }
  vector_free(&vec);
  deleteFile("../tocheck.txt");
}

/**
  * Updates average time
  *
  * @return nothing
*/
void updAverageTime() {
  if(checks.size == lastStored) {
    return;
  }
  float toSet = 0;
  long realCount = 0;
  for(int i = 0; i < checks.size; ++i) {
    if(vector_getfloat(&checks, i) != 666) {
      toSet += vector_getfloat(&checks, i);
      realCount++;
    }
  }
  averageCheckTime = toSet /= realCount;
  lastStored = realCount;
}

//Oh how i wish C supported function overloading.

/**
  * Runs the random test on currentCheck.
  *
  * @param iterations for tests.
  * @param The float vector
  * @return nothing.
*/
void runRandomTests(int iters, FloatVector *vec) {
  if(checkSignature(currentCheck)) {
    int maxValues = (iters * 50) + 50;
    void *handle = load(currentCheck);
    int (*setseedrandom)();
    float (*randfloat) ();
    setseedrandom = dlsym(handle, "setseedrandom");
    randfloat = dlsym(handle, "randfloat");
    for(int i = 50; i < maxValues; i += 50) {
      int ii = setseedrandom();
      vector_appendfloat(&randomSeeds, (float) ii);
      vector_appendfloat(vec, randfloat());
      vector_appendfloat(vec, randfloat());
    }
    vector_appendfloat(vec, averageFloatVector(randomSeeds));
    clearFloatVector(&randomSeeds);
    unload(handle);
  }
}

/**
  * Runs the random test on value passed.
  * @FunctionOverload#1
  *
  * @param iterations for tests.
  * @param path to handle to load.
  * @param The float vector
  * @return nothing.
*/
void runRandomTests0(int iters, char* path, FloatVector *vec) {
  if(checkSignature(path)) {
    int maxValues = (iters * 50) + 50;
    void *handle = load(path);
    int (*setseedrandom)();
    float (*randfloat) ();
    setseedrandom = dlsym(handle, "setseedrandom");
    randfloat = dlsym(handle, "randfloat");
    for(int i = 50; i < maxValues; i += 50) {
      int ii = setseedrandom();
      vector_appendfloat(&randomSeeds, (float) ii);
      vector_appendfloat(vec, randfloat());
      vector_appendfloat(vec, randfloat());
    }
    vector_appendfloat(vec, averageFloatVector(randomSeeds));
    clearFloatVector(&randomSeeds);
    unload(path);
  }
}

/**
  * Runs the random test on value passed.
  * @FunctionOverload#2
  *
  * @param Minimum Iter Value
  * @param Maximum Iter Value
  * @param path to handle to load.
  * @param The float vector
  * @return nothing.
*/
void runRandomTests1(int minIter, int maxIter, char* path, FloatVector *vec) {
  if(checkSignature(path)) {
    void *handle = load(path);
    int (*setseedrandom)();
    float (*randfloat) ();
    setseedrandom = dlsym(handle, "setseedrandom");
    randfloat = dlsym(handle, "randfloat");
    for(int i = minIter; i < maxIter; i += 50) {
      int ii = setseedrandom();
      vector_appendfloat(&randomSeeds, (float) ii);
      vector_appendfloat(vec, randfloat());
      vector_appendfloat(vec, randfloat());
	  }
    vector_appendfloat(vec, averageFloatVector(randomSeeds));
    clearFloatVector(&randomSeeds);
    unload(handle);
  }
}

/**
  * Runs the random test on value passed.
  * @FunctionOverload#3
  *
  * @param Minimum Iter Value
  * @param Maximum Iter Value
  * @param Iter Value to skip by
  * @param path to handle to load.
  * @param The float vector
  * @return nothing.
*/
void runRandomTests2(int minIter, int maxIter, int iterDiff, char* path, FloatVector *vec) {
  if(checkSignature(path)) {
    void *handle = load(path);
    int (*setseedrandom)();
    float (*randfloat) ();
    setseedrandom = dlsym(handle, "setseedrandom");
    randfloat = dlsym(handle, "randfloat");
	  for(int i = minIter; i < maxIter; i += iterDiff) {
		  int ii = setseedrandom();
      vector_appendfloat(&randomSeeds, (float) ii);
      vector_appendfloat(vec, randfloat());
      vector_appendfloat(vec, randfloat());
	  }
    vector_appendfloat(vec, averageFloatVector(randomSeeds));
    clearFloatVector(&randomSeeds);
    unload(handle);
  }
}

/**
  * Runs the set test on currentCheck.
  *
  * @param iterations for tests.
  * @param Float vector to add too
  * @param The fixed seed
  * @return nothing.
*/
void runSetTests(int iters, FloatVector *vec, int seed) {
  if(checkSignature(currentCheck)) {
    int maxValues = (iters * 50) + 50;
    void *handle = load(currentCheck);
    float (*randfloat) ();
    void (*setseedfixed) (int);
    randfloat = dlsym(handle, "randfloat");
    setseedfixed = dlsym(handle, "setseedfixed");
    for(int i = 50; i < maxValues; i += 50) {
      
    }
  }
}

/**
  * Runs the set test on value passed.
  * @FunctionOverload#1
  *
  * @param iterations for tests.
  * @param path to handle to load.
  * @param The float vector
  * @param The fixed seed
  * @return nothing.
*/
void runSetTests0(int iters, char* path, FloatVector *vec, int seed) {

  int maxValues = (iters * 50) + 50;
  for(int i = 50; i < maxValues; i += 50) {
    
  }
}

/**
  * Runs the set test on value passed.
  * @FunctionOverload#2
  *
  * @param Minimum Iter Value
  * @param Maximum Iter Value
  * @param path to handle to load.
  * @param The float vector
  * @param The fixed seed
  * @return nothing.
*/
void runSetTests1(int minIter, int maxIter, char* path, FloatVector *vec, int seed) {

  for(int i = minIter; i < maxIter; i += 50) {

  }
}

/**
  * Runs the set test on value passed.
  * @FunctionOverload#3
  *
  * @param Minimum Iter Value
  * @param Maximum Iter Value
  * @param Iter Value to skip by
  * @param path to handle to load.
  * @param The float vector
  * @param The fixed seed
  * @return nothing.
*/
void runSetTests2(int minIter, int maxIter, int iterDiff, char* path, FloatVector *vec, int seed) {
  for(int i = minIter; i < maxIter; i += iterDiff) {
    
  }
}

/**
  * Basically the update process
  *
  * @return Nothng.
*/
void runTick() {
	while(true) {
    clock_t start = clock(), diff;
		updCheckQueue(addStrings(2, BASE_DIR, "../tocheck.txt"));
		if(checkQueue.size > 0) {
			char* c = vector_get(&checkQueue, 0);
			currentCheck = c;
			deleteAndShift(1, checkQueue);
      FloatVector toWrite;
      vector_initfloat(&toWrite);
      //Doing this here makes a double signature check happen. Which isn't neccesarlly a bad thing, and could prove useful.
      char* newF = prepareFile(currentCheck);
      if(newF != NULL) {
       currentCheck = newF;
			 if(checkLoad(currentCheck) == 2) {
          runRandomTests(1000000000, &toWrite);
          for(int i = 50; i < 1000000000; ++i) {
            runSetTests(1000000000, &toWrite, i);
          }
        }else if(checkLoad(currentCheck) == 1) {
          runRandomTests(1000000000, &toWrite);
        }
      }
      
		}
    diff = clock() - start;
    checkTime = (float) diff;
    vector_appendfloat(&checks, checkTime);
    updAverageTime();
	}
}

/**
  * Gets called on application start. Basically init's everything
  * 
  * @param startup arg count
  * @param Startup args
  * @return Nothing.
*/
int main(int argc, char **argv) {
	if(argc == 0) {
    lastStored = 0;
    checkTime = 0;
		vector_init(&checkQueue);
		vector_initfloat(&checks);
    vector_initfloat(&randomSeeds);
    vector_initfloat(&overallRandoms);
		while(true) {
			runTick();
		}
		vector_free(&checkQueue);
    vector_freefloat(&checks);
    vector_freefloat(&randomSeeds);
    vector_freefloat(&overallRandoms);
		return 0;
	}else if(argc == 2) {

	}else if(argc == 3) {

	}else if(argc == 4) {

	}else if(argc == 5) {

  }else {
		printf("Invalid Usage!! Check help docs!");
	}
}
