/*
 * loader.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <unistd.h>
#include "loader.h"
#include "utils.h"

/**
  * Returns File loaded properties
  *
  * @param Full path relative to the program to .so to test.
  * @return 0 If cannot load, 1 If load Random Tests, 2 If can load fix tests
*/
int checkLoad(char *path) {
	void *handle;
	int (*setseedrandom)();
	int (*randint) ();
	double (*randdouble) ();
	float (*randfloat) ();
	char* error;
	handle = dlopen(path, RTLD_NOW);
	if(!handle) {
		fprintf (stderr, "%s\n", dlerror());
        return 0;
	}
	dlerror();
	setseedrandom = dlsym(handle, "setseedrandom");
	randint = dlsym(handle, "randint");
	randdouble = dlsym(handle, "randdouble");
	randfloat = dlsym(handle, "randfloat");
    if ((error = dlerror()) != NULL)  {
        fprintf (stderr, "%s\n", error);
        dlclose(handle);
        return 0;
    }
    void(*setseedfixed)(int);
    setseedfixed = dlsym(handle, "setseedfixed");
    if(setseedfixed) {
    	dlclose(handle);
    	return 2;
    }else {
    	dlclose(handle);
    	return 1;
    }
}

/**
  * Returns a "handle" to an opened .so The .so should first be checked with checkLoad(char* path) to make sure no errors occur.
  *
  * @param That full path relative to the program of the .so
  * @return Handle to the opened .so
*/
void *load(char *path) {
	void *handle = dlopen(path, RTLD_NOW);
	if(!handle) {
		fprintf (stderr, "%s\n", dlerror());
		return NULL;
	}
	return handle;
}

/**
  * Unloads a .so file that we are done testing.
  *
  * @param the "handle" to the opened .so
  * @return Nothing
*/
void unload(void *handle) {
	dlclose(handle);
}

/**
  * Checks the Signature of the file to make sure it is verified to run.
  *
  * @param the path of the file to check.
  * @return wether the file has a valid signature.
*/
bool checkSignature(char* path) {
  return false;
}

/**
  * Prepares, and checks the file. Basically copies it into it's own directory. 
  * (Should all be compiled into one .so (can download any dependencies if needed during runtime)).
  * Process: Copy to own directory -> link possible system dependencies -> chmod. 
  * TOCHECK: The signature SHOULD copy. Seeing as how it is literally a direct copy.
  *
  * @param The path to the file
  * @return The path to the new ready file.
*/
char* prepareFile(char* path) {
  if(!checkSignature(path)) return NULL;
  char* pathSplit = splitByDelimeter(path, "/");
  char* modDir = NULL;
  for(int i = 0; i < sizeof(pathSplit) - 2; ++i) {
    if(modDir != NULL) {
      char* holder = modDir + pathSplit[i];
      modDir = holder;
    }else {
      modDir = &pathSplit[i];
    }
  }
  char* cwd = malloc(1024);
  getcwd(cwd, 1024);
  system(addStrings(2, "cd ", modDir));
  system(addStrings(2, "mkdir ", pathSplit[(sizeof(pathSplit) - 2)]));
  system(addStrings(6, "cp " , path , " ./" , pathSplit[(sizeof(pathSplit) - 2)] , "/" , pathSplit[(sizeof(pathSplit) - 1)]));
  system(addStrings(3, "ln -s /dev/urandom " , modDir , "/dev/urandom"));
  system(addStrings(3, "ln -s /dev/hwrng " , modDir , "/dev/hwrng"));
  system(addStrings(3, "ln -s /dev/random " , modDir , "/dev/random"));
  system(addStrings(4, "chmod 666 ./" , pathSplit[(sizeof(pathSplit - 2))] , "/" , pathSplit[(sizeof(pathSplit) - 1)]));
  system(addStrings(2, "cd " , cwd));
  return (addStrings(4, modDir , pathSplit[(sizeof(pathSplit - 2))] , "/" , pathSplit[(sizeof(pathSplit) - 1)]));
}
