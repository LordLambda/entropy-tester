**Testodom**
==============

**Entropy Testing taken to the next level.**
--------------

In this Directory all the C/C++ Files used to create the tester for PRNGs. The tester is written in base C though feel free to modify it for personal use. The tester works only on Linux Distributions, and as such loads ".so" files. It will then check if the ".so" file has the neccessary functions to run the test, and if so adds it to the queue. This tester is ment to be run as a background process it will check a file for the full path of a .so file every iteration. It will then delete the PRNG off the file as to not double test the same algorithim, and not having to parse a big file after a while.

This program actually doesnt do the Encoding of the blobs to bzip2. That is done in a python script. Instead the blob files here are base64 encodings of the ascii values of the blob. The python script located in the utils section as BlobEncoder.py will scan the output directory here, and compress the file, and delete the non-compressed version.

You can make the tester by executing:
```
make
sudo make install
./install
```

Credits:

- Revolution (Myself)
- Aladdin Enterprises for their C MD5 implementation
- Frank4DD For the Base64 C implementation