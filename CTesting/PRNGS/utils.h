/*
 * Utils.h
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "charvector.h"
#include "floatvector.h"
#include <setjmp.h>
#include <stdbool.h>

int endWith(const char *haystack, const char *needle);

bool isInQuadrant(float f, float f2);

char* md5String(char* s);

int currentMicroTime();

void longToStr(long n, char* pStr);

Vector stringToAsciiValue(char* str);

char* charVecToString(Vector dat, char* delim);

Vector deleteAndShift(int numToDel, Vector vec);

void clearFloatVector(FloatVector *vec);

void clearCharVector(Vector *vec);

float averageFloatVector(FloatVector vec);

char* splitByDelimeter(char* toSplit, char* delimeter);

char* addStrings(int numOfArgs, char* one, ...);
