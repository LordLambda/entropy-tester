/*
 * FloatVector.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include "floatvector.h"

/**
  * Initalizes an FloatVector after it's defined
  *
  * @param The FloatVector to initalize.
  * @return Nothing
*/
void vector_initfloat(FloatVector *vector) {
  vector->size = 0;
  vector->capacity = VECTOR_INITIAL_CAPACITY;
  vector->data = malloc(sizeof(int) * vector->capacity);
}

/**
  * Appends a value to a FloatVector.
  *
  * @param The FloatVector to append too.
  * @param The value to append
  * @return Nothing.
*/
void vector_appendfloat(FloatVector *vector, float value) {
  vector_double_capacity_if_fullfloat(vector);
  vector->data[vector->size++] = value;
}

/**
  * Returns the value in location of vector.
  *
  * @param The FloatVector to get a value from.
  * @param The place in the vector to get from.
  * @return The Value in the specified location.
*/
float vector_getfloat(FloatVector *vector, int index) {
  if (index >= vector->size || index < 0) {
    printf("Index %d out of bounds for vector of size %d\n", index, vector->size);
    exit(1);
  }
  return vector->data[index];
}

/**
  * Sets a current value in a vector
  *
  * @param The FloatVector to modify
  * @param The value to change
  * @param The value to replace with
  * @return Nothing
*/
void vector_setfloat(FloatVector *vector, int index, float value) {
  while (index >= vector->size) {
    vector_appendfloat(vector, 666);
  }
  vector->data[index] = value;
}

/**
  * Doubles a vectors size if the vector is full.
  *
  * @param The vector size to double.
  * @return Nothing
*/
void vector_double_capacity_if_fullfloat(FloatVector *vector) {
  if (vector->size >= vector->capacity) {
    vector->capacity *= 2;
    vector->data = realloc(vector->data, sizeof(int) * vector->capacity);
  }
}

/**
  * Frees the data space reserved by a vector.
  *
  * @param the vector to free.
  * @return Nothing
*/
void vector_freefloat(FloatVector *vector) {
  free(vector->data);
}
