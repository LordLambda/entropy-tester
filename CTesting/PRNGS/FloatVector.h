/*
 * FloatVector.h
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define VECTOR_INITIAL_CAPACITY 4

#ifndef FloatVectorDef

#define FloatVectorDef

typedef struct {
  int size;
  int capacity;
  float *data;
} FloatVector;

#endif

void vector_initfloat(FloatVector *vector);

void vector_appendfloat(FloatVector *vector, float value);

float vector_getfloat(FloatVector *vector, int index);

void vector_setfloat(FloatVector *vector, int index, float value);

void vector_double_capacity_if_fullfloat(FloatVector *vector);

void vector_freefloat(FloatVector *vector);
