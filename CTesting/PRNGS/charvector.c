/*
 * CharVector.c
 * 
 * Copyright 2014 Revolution <revolution@potateos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include "charvector.h"
#include <stdlib.h>
#include <stdio.h>

/**
  * Initializes a vector.
  *
  * @param The vector to initalize
  * @return Nothing
*/
void vector_init(Vector *vector) {
  vector->size = 0;
  vector->capacity = VECTOR_INITIAL_CAPACITY;
  vector->data = malloc(sizeof(int) * vector->capacity);
}

/**
  * Adds a value to a vector.
  *
  * @param Vector to add too.
  * @param The value to add
  * @return Nothing
*/
void vector_append(Vector *vector, char* value) {
  vector_double_capacity_if_full(vector);
  vector->data[vector->size++] = value;
}

/**
  * Returns a value from a vector in a certain location
  *
  * @param Vector to get value from.
  * @param The index of the value to get.
  * @return The value in specified index
*/
char* vector_get(Vector *vector, int index) {
  if (index >= vector->size || index < 0) {
    printf("Index %d out of bounds for vector of size %d\n", index, vector->size);
    exit(1);
  }
  return vector->data[index];
}

/**
  * Sets a value in a location. "Replacing it".
  *
  * @param The vector to set the value
  * @param The index for the old value to be replaced
  * @param The new value.
  * @return Nothing.
*/
void vector_set(Vector *vector, int index, char* value) {
  while (index >= vector->size) {
    vector_append(vector, NULL);
  }
  vector->data[index] = value;
}

/**
  * Doubles a vector's size if it's full.
  *
  * @param The vector to check, or possibly double.
  * @return Nothing
*/
void vector_double_capacity_if_full(Vector *vector) {
  if (vector->size >= vector->capacity) {
    vector->capacity *= 2;
    vector->data = realloc(vector->data, sizeof(int) * vector->capacity);
  }
}

/**
  * Frees a space reserved by a vector.
  *
  * @param Vector to free
  * @return Nothing.
*/
void vector_free(Vector *vector) {
  free(vector->data);
}
