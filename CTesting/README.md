**Testodom**
==============

**Entropy Testing taken to the next level.**
--------------

In this Directory all the C/C++ Files used to create the tester are loaded. The tester itself is written in base C, and is targeted for Linux Distributions. Mainly for random reseeds PRNGs, and such have access to /dev/urandom. The distro the Tester is used on Server side is Arch Linux, and as such is only guarenteed to work on Arch Linux Distributions. Support for windows will likely never be added. Please do not ask for it.