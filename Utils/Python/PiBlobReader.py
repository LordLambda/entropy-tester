import bz2, os, fileinput, re

def averageBlobValues(listt):
	total = 0
	count = 0
	for double in listt:
		total += double
		count += 1
	return (total / count)

def genLaTeXPiData(algoName, piAverage):


def getnum(xx):
	return re.findall(r"[-+]?\d*\.\d+|\d+", xx)

def filesInDir(dir, listt):
	return len([name for name in os.listdir(dir) if os.path.isfile(name) and name not in listt])

def runTick():
	filesChecked = []
	while(True):
		if(filesInDir('../compressedBlobsPi/', filesChecked) > 0):
			onlyfiles = [ f for f in listdir('../compressedBlobsPi/') if isfile(join('../compressedBlobsPi/',f)) and join('../compressedBlobsPi/',f) not in  filesChecked]
			for value in onlyfiles:
				unbizzpd = ''
				with open(value, 'r') as content_file:
    				content = content_file.read()
    				unbizzpd = bz2.decompress(content)
    				content_file.close()
    			split = unbizzpd.split('\n')
    			isInBlob = False
    			blobNumber = 0
    			blobName = ''
    			currentBlobData = []
    			seedCommon = ''
    			for line in split:
    				if(line.startswith('--')):
						if(isInBlob):
							blobsRead += 1
							isInBlob = False
							sss = "Ending Blob: {0}".format(blobNumber)
							print(sss)
							genLaTeXPiData(blobName, averageBlobValues(currentBlobData))
							currentBlobData = []
						else:
							isInBlob = True
							holder = re.findall('\d+', line)
							blobNumber = int(holder[0])
							s = "Starting Blob: {0}".format(blobNumber)
							print(s)
					elif(line.startswith("In Quadrant")):
						#Do something here.
						ssss = line
					elif(line.startswith("X:")):
						if(isInBlob):
							ll = getnum(line)
							currentBlobValues.append(float(ll[0]))
							currentBlobValues.append(float(ll[1]))
					elif(line.startswith("MostCommonSeed")):
						seedCommon = line
					else:
						blobName = line

def Main():
	runTick()

if __name__ == '__main__':
	Main()