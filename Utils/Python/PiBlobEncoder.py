import bz2, os, hashlib, time

def genFileName(headDir):
	m = hashlib.md5()
	m.update(str(int(round(time.time() * 1000))))
	return headDir + m.hexdigest()

def filesInDir(dir):
	return len([name for name in os.listdir(dir) if os.path.isfile(name)])

def runTick():
	while(True):
		if(filesInDir('../uncomp/Pi') > 0):
			onlyfiles = [ f for f in listdir('../uncomp/Pi') if isfile(join('../uncomp/Pi',f)) ]
			for value in onlyfiles:
				bzipped = ''
				with open(value, 'r') as content_file:
    				content = content_file.read()
    				bzipped = bz2.compress(content)
    				content_file.close()

    			with open(genFileName('../compressedBlobsPi'), 'w+') as new_f:
    				new_f.write(bzipped)
    				new_f.close()
    			os.remove(value)
    	if(filesInDir('../uncomp/StringRNG') > 0):
    		onlyfiles = [ f for f in listdir('../uncomp/StringRNG') if isfile(join('../uncomp/StringRNG',f)) ]
			for value in onlyfiles:
				bzipped = ''
				with open(value, 'r') as content_file:
    				content = content_file.read()
    				bzipped = bz2.compress(content)
    				content_file.close()

    			with open(genFileName('../compressedBlobsStringRNG'), 'w+') as new_f:
    				new_f.write(bzipped)
    				new_f.close()
    			os.remove(value)
    	if(filesInDir('../uncomp/Enc') > 0):
			onlyfiles = [ f for f in listdir('../uncomp/Enc') if isfile(join('../uncomp/Enc',f)) ]
			for value in onlyfiles:
				bzipped = ''
				with open(value, 'r') as content_file:
    				content = content_file.read()
    				bzipped = bz2.compress(content)
    				content_file.close()

    			with open(genFileName('../compressedBlobsEnc'), 'w+') as new_f:
    				new_f.write(bzipped)
    				new_f.close()
    			os.remove(value)    		


def main():
	runTick()

if __name__ == "__main__":
	main()