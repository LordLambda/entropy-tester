import sys

'''
function checkImport:
	@param module to check if it can be imported
	@return Module can be imported  or not
'''
def checkImport(modulename):
	try:
		__import__(modulename)
		modulename.genRandSeed()
		modulename.genDouble()
		modulename.genFloat()
		del modulename
		return True
	except:
		return False

'''
function importModule:
   @param the module to import.
   @return the imported module
'''
def importModule(modulename):
	__import__(modulename)
	return modulename

'''
function unloadModule:
    @param The module to unload.
    @return Nothing
'''
def unloadModule(modulename):
	del modulename